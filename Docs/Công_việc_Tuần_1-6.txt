Công việc Tuần 1 - Tuần 6:

Tuần 1:
- B1: Chuẩn hoá thông tin bài toán bao gồm: Chưa có tài liệu chuẩn hoá, việc gọi tên bài toán vẫn dựa trên tên gốc (family name) của đối tượng mà không chỉ định được rõ bài toán đi kèm. VD: Bài toán nguồn AC, trần nhà,…. Công việc bao gồm: Đánh mã số bài toán, xây dựng từ điển bài toán được chuẩn hoá làm ngôn ngữ thống nhất chung gồm Tên - mô tả - tiêu chuẩn kỹ thuật thu thập dữ liệu - yêu cầu cần đạt được - phân  loại bài toán - nguyên lí giải quyết - đánh giá tính khả thi - tóm tắt/báo cáo kỹ thuật -  kết quả triển khai . Bao gồm các bài toán cũ và đề xuất mới.
- B2: Xây dựng quy trình trao đổi thông tin trong team, đào tạo sử dụng công cụ, nguyên tắc phối hợp
- B3: Khảo sát hiện trường để hiệu chỉnh các thông tin.
- B4: Khảo sát các nghiên cứu và mô hình kinh doanh liên quan (Related work) trong nước và trên thế giới (keyword: telecommunications tower)

Tuần 2:
- B4: Kết luận và hoàn thiện kế hoạch giai đoạn 2 (Tiêu chuẩn kỹ thuật mới, các task mới, công nghệ mới) - Trình bày với BGĐ xin phép triển khai
- B5: Chuẩn hoá dataset: Dữ liệu đang phân mảnh, không có cấu trúc, đặt tên theo cảm tính, mất nhãn > Cần thống nhất 1 format chung cho data đối với từng loại bài toán. Sau đó phân quyền để các thành viên đưa về sử dụng cho model của mình
Đường dẫn lưu trữ: 
- B6: Thống nhất quy trình với bên nghiệp vụ vể điều chỉnh phần mềm tương ứng

Tuần 3-4-5:
- B7: Phân tích công nghệ và phân công triển khai công nghệ đề xuất (Hoàn tất đánh nhãn dữ liệu, huấn luyện mô hình)
- B8: Tích hợp nBox, thử nghiệm và hiệu chỉnh

Tuần 6:
- B9: Báo cáo và đánh giá
- B10: Ban hành và triển khai chính thức
